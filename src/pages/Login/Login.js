import "./Login.css";
import React ,{ useState } from "react";
import axios from "axios";
import TextField from '@mui/material/TextField';


export default function Login () {
  const [user, setUser] = useState('');
  const [pass, setPass] = useState('');
  const [error,setError] = useState('');

  const handleChange =(e)=>{
    var bodyFormData = new FormData();
    bodyFormData.append('user', user)
    bodyFormData.append('pass', pass)

    const config = {     
      headers: { 'content-type': 'multipart/form-data' }
    }
    axios.post('https://api.fiindev.com/login',bodyFormData,config)
    .then(response => {
        if (response.data.token) {
          localStorage.setItem("user", JSON.stringify(response.data))
          localStorage.setItem("nameuser", user);
          // window.location.href = "/search-customer";
          console.log(response.data.token)
        }
        if(response.data === null) {
          setError('Invalid username')
          console.log(error)
        }
      })
  };
  
  return (
  <div className="bg">
    <div className="d-flex justify-content-center align-items-center gradient">
      <div className="form" >
        <div className="pages-header px-3 py-1"><h2 style={{color: 'white'}}>LOGIN</h2></div>
        <div className="mb-6"><h4>WELCOME</h4></div>
        <div className="pages-detail mb-4 px-6">Please use the form to sign-in</div>
        <div className="mb-3">
          <div  className="mb-4">
            <div className="mb-3">
              <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">
                      <i className="pi pi-user"></i>
                  </span>
                  <TextField className="col-fixed w-9rem"
                    label="username"
                    defaultValue=""
                    type="user"
                    name="user"
                    onChange={(e)=>setUser(e.target.value)}
                  />
              </div>
            </div>
              <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">
                      <i className="pi pi-lock"></i>
                  </span>
                  <TextField className="col-fixed w-9rem"
                    label="password"
                    defaultValue=""
                    type="password"
                    name="password"
                    onChange={(e)=>setPass(e.target.value)}
                    onKeyPress={(e) => e.key === 'Enter' && handleChange() }
                  />
              </div>
          </div>
          <div className="d-grid" >
            <button className="btn btn-primary" type="submit" onClick={handleChange}>Login</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  )
};
