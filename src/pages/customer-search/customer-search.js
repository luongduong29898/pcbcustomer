
import './customer-seacrch.css'
import { Table } from "semantic-ui-react";
import { IconButton , Collapse } from '@mui/material';
import { useState } from 'react';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowDown'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowUp'
import { Box, Button } from "@material-ui/core";



function ContractSearch () {
  const data = JSON.parse(localStorage.getItem("dataname"));
  const [open, setOpen] = useState(true);
  const [open1, setOpen1] = useState(true);
  const [open2, setOpen2] = useState(true);
  const [open3, setOpen3] = useState(true);
  const [open4, setOpen4] = useState(true);
  const [open5, setOpen5] = useState(true);
  const [open6, setOpen6] = useState(true);
  const [open7, setOpen7] = useState(true);
  const [open8, setOpen8] = useState(true);
  const [open9, setOpen9] = useState(true);
  const [open10, setOpen10] = useState(true);
  const [open11, setOpen11] = useState(true);
  const [open12, setOpen12] = useState(true);
  const [open13, setOpen13] = useState(true);
  const [open14, setOpen14] = useState(true);
  const [open15, setOpen15] = useState(true);


    const getFormattedDate1 = (dateStr) => {
      const date = new Date(dateStr) ;
      return date.toLocaleDateString();
    }

    const handlePrint = () => {
      window.print();
    };  

    const CurrencyFormat = require('react-currency-format');
    
 
  return (
        <div className="datatable-filter">
          {/* searchinput */}
          <Box>
            <Button variant="contained" onClick={handlePrint}>
              Print
            </Button>
          </Box>

          <div className="card">
            <h1 className='Header'>BÁO CÁO TÍN DỤNG KHÁCH HÀNG</h1>
            <div className='Information' content='Responsive Table'>
              <h3>THÔNG TIN KHÁCH HÀNG</h3>
              
              {/* Identical */}
              <div className="Identical">
                <Table.HeaderCell>Thông tin định danh
                  <IconButton aria-label="expand row"  onClick={() => setOpen(!open)}>
                      {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                  </IconButton>
                </Table.HeaderCell>
                <Collapse in={open}>
                <Table>  
                  <Table.Header> </Table.Header>
                  <Table.Body colSpan={6}>
                    <Table.Row>
                      <Table.Cell>Mã Khách hàng</Table.Cell>
                      <Table.Cell>
                        {data.Identical.sid}
                      </Table.Cell>
                      <Table.Cell>Type</Table.Cell> 
                      <Table.Cell>
                        {data.Identical.type}
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Tên khách hàng</Table.Cell>
                      <Table.Cell>
                        {data.Identical.name}
                      </Table.Cell>
                      <Table.Cell>Giới tính</Table.Cell>
                      <Table.Cell>
                        {data.Identical.gender}
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Ngày sinh</Table.Cell>
                      <Table.Cell>
                        {getFormattedDate1(data.Identical.dob)}
                      </Table.Cell>
                      <Table.Cell>Nơi sinh</Table.Cell>
                      <Table.Cell>
                        {data.Identical.birthtown}
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Nơi sinh(Quốc Gia)</Table.Cell>
                      <Table.Cell>
                        {data.Identical.birthcountry}
                      </Table.Cell>
                      <Table.Cell>Mã số thuế</Table.Cell>
                      <Table.Cell>-</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Số CMND/CCCD</Table.Cell>
                      <Table.Cell>
                        {data.Identical.pin}
                      </Table.Cell>
                      <Table.Cell>Số CMND/CCCD 2</Table.Cell>
                      <Table.Cell>- </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                  
                </Table>
              </Collapse>
              </div>

              {/* Address */}
              <div className="Address">
                <Table.HeaderCell>Thông tin địa chỉ
                  <IconButton aria-label="expand row"  onClick={() => setOpen1(!open1)}>
                      {open1 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                  </IconButton>
                </Table.HeaderCell>
                <Collapse in={open1}>
                <Table singleLine >
                  <Table.Header>
                    
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>Địa chỉ thường trú</Table.Cell>
                        <Table.Cell>
                          {data.Addresses.map((item, i) => (
                            <tr key={i}>
                              <td>{item.namaddressname}</td>
                            </tr>
                          ))}
                        </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
                </Collapse>
              </div>

               
              <div className="Document">
                <Table.HeaderCell>Giấy tờ cá nhân
                  <IconButton aria-label="expand row"  onClick={() => setOpen2(!open2)}>
                      {open2 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                  </IconButton>
                </Table.HeaderCell>
                {
              (!(data.Documents.map((item) => (item.documentid)))) && 
                <Collapse in={open2}>
                <Table singleLine >
                  <Table.Header>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.HeaderCell>Loại giấy tờ</Table.HeaderCell>
                      <Table.HeaderCell>Số</Table.HeaderCell>
                      <Table.HeaderCell>Ngày Cấp</Table.HeaderCell>
                      <Table.HeaderCell>Nơi Cấp</Table.HeaderCell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Hộ Chiếu</Table.Cell>
                      <Table.Cell>
                        {data.Documents.map((item, i) => (
                          <tr key={i}>
                            <td>{item.documentid}</td>
                          </tr>
                        ))}
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Số Hộ Khẩu</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Giấy phép lái xe</Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
                </Collapse>
                }
              </div>

              {/* Contact */}
              <div className="Contact">
                <Table.HeaderCell>Thông tin liên hệ
                  <IconButton aria-label="expand row"  onClick={() => setOpen3(!open3)}>
                      {open3 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                  </IconButton>
                </Table.HeaderCell>
                <Collapse in={open3}>
                <Table >
                  <Table.Header>
                    
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                    <Table.Cell >Số điện thoại</Table.Cell>
                        <Table.Cell >
                          {data.Contacts.map((item, i) => (
                            <tr key={i}>
                              <td>{item.phone_num}</td>
                            </tr>
                          ))}
                        </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
                </Collapse>
              </div>
            </div>
            
            {/* Contract */}
            <div className="Contract">
              <h3>THÔNG TIN HỢP ĐỒNG VAY</h3>
              {/* Information */}
              <h5>Thông tin chung  </h5>
              
              <Table singleLine >
                <Table.Header>
                  
                </Table.Header>
                <Table.Body>
                  <Table.Row>
                    <Table.Cell>Số lượng hợp đồng</Table.Cell>
                    <Table.Cell>
                      {data.Contracts.Total_contract}
                    </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Số TCTD có quan hệ tín dụng</Table.Cell>
                      <Table.Cell>
                        {data.Contracts.total_institutes}
                      </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Mã tiền tệ</Table.Cell>
                      <Table.Cell>
                        {data.Contracts["Currency code"]}
                      </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Tình trạng nợ cao nhất trong 3 tháng gần nhất</Table.Cell>
                      <Table.Cell>
                        {data.Contracts["Worst Status"]}
                      </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>

              {/* Number of Contract */}
              <Table singleLine >
                <Table.Header>
                  <h5>Số lượng hợp đồng</h5>
                </Table.Header>
                <Table.Body>
                  <Table.Row>
                    <Table.HeaderCell>Nhóm hợp đồng vay</Table.HeaderCell>
                    <Table.HeaderCell>Yêu cầu</Table.HeaderCell>
                    <Table.HeaderCell>Từ chối</Table.HeaderCell>
                    <Table.HeaderCell>Từ bỏ</Table.HeaderCell>
                    <Table.HeaderCell>Đang tồn tại</Table.HeaderCell>
                    <Table.HeaderCell>Chấm dứt</Table.HeaderCell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Vay thông thường</Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Instalments.request}
                      </Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Instalments.Refused}
                      </Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Instalments.Renounced}
                      </Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Instalments.Living}
                      </Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Instalments.Terminated}
                      </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Vay thấu chi</Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Notinstalments.request}
                      </Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Notinstalments.Refused}
                      </Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Notinstalments.Renounced}
                      </Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Notinstalments.Living}
                      </Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Notinstalments.Terminated}
                      </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Thẻ tín dụng</Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Creditcards.request}
                      </Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Creditcards.Refused}
                      </Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Creditcards.Renounced}
                      </Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Creditcards.Living}
                      </Table.Cell>
                      <Table.Cell>
                        {data.Contracts.Creditcards.Terminated}
                      </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
              
            </div>

            {(data.Contracts.Instalments.Living) !== 0 &&  
              <div className='Instalments'>
              <h3>I. NHÓM: VAY THÔNG THƯỜNG 
                <IconButton aria-label="expand row"  onClick={() => setOpen4(!open4)}>
                      {open4 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                </IconButton>
              </h3>
              <Collapse in={open4}>
              <Table singleLine >
                <Table.Header>
                  
                  <Table.Row>
                    <Table.HeaderCell>Tổng tiền - vay thông thường</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                
                <Table.Body>
                  <Table.Row><Table.HeaderCell></Table.HeaderCell>
                    <Table.HeaderCell>Vai trò vay chính/đồng vay</Table.HeaderCell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Số tiền gốc và lãi phải trả trung bình các tháng tiếp theo</Table.Cell>
                      <Table.Cell> 
                        <CurrencyFormat 
                        className = "money"
                        value = {data.Contracts.Instalments.Amount_of_monthly_installments} 
                        displayType={'text'}
                        thousandSeparator={true} />
                      </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Số tiền gốc và lãi còn nợ</Table.Cell>
                      <Table.Cell>
                      <CurrencyFormat 
                        className = "money"
                        value = {data.Contracts.Instalments.Total_remaining_amount}
                        displayType={'text'}
                        thousandSeparator={true} />
                      </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Số tiền gốc và lãi còn nợ (theo kế hoạch)</Table.Cell>
                      <Table.Cell>
                        <CurrencyFormat 
                          className = "money"
                          value = {data.Contracts.Instalments.Amount_of_Remaining_Installments}
                          displayType={'text'}
                          thousandSeparator={true} />
                      </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Số tiền gốc và lãi nợ quá hạn</Table.Cell>
                      <Table.Cell>
                        <CurrencyFormat 
                        className = "money"
                        value = {data.Contracts.Instalments.Amount_of_unpaid_due_installments}
                        displayType={'text'}
                        thousandSeparator={true} />
                      </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
                
              {data.Contracts.Instalments.Instalment_list.map((item, i) => (
                <div>
                <Table.HeaderCell> Chi tiết: Vay mua nhà
                  <IconButton aria-label="expand row"  onClick={() => setOpen5(!open5)}>
                        {open5 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                  </IconButton>
                </Table.HeaderCell>
                <Collapse in={open5}>
                <Table singleLine>
                  <Table.Header></Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>Mã TCTD</Table.Cell>
                      <Table.Cell>
                        <tr key={i}><td>{item.Contract.organizationowner}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Loại TCTD</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.organizationtype}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Giai đoạn HĐV</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.contractphase}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Nhóm HĐV</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.contracttype}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Loại HĐV</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.operationtype}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Ngày yêu cầu vay</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{getFormattedDate1(item.Contract.datfinancingapplication)}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Ngày bắt đầu HĐV</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{getFormattedDate1(item.Contract.datstartingcontract)}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Ngày kết thúc theo kế hoạch</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{getFormattedDate1(item.Contract.datclosingcontractprevisional)}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Nhóm nợ</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.contractstatus}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Ngày chốt dữ liệu gần nhất</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{getFormattedDate1(item.Contract.datlastupdatebatch)}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Mã khu vực cấp tín dụng</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.areaowner}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Ngày kết thúc thực tế</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{getFormattedDate1(item.Contract.datclosingcontractreal)}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Loại tiền tệ</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.creditcurrency}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Cờ tái cơ cấu</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.reorganizedcredit}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Mã Chi nhánh cấp tín dụng</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.branchowner}</td></tr>
                      </Table.Cell>
                      <Table.Cell> </Table.Cell>
                      <Table.Cell> </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
                </Collapse>

                  <Table.HeaderCell> Chi tiết: Vay mua sắm có thỏa thuận với đại lý bán lẻ
                    <IconButton aria-label="expand row"  onClick={() => setOpen6(!open6)}>
                          {open6 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                  </Table.HeaderCell>
                  <Collapse in={open6}>
                <Table>
                  <Table.Header>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>Số tiền yêu cầu vay</Table.Cell>
                      <Table.Cell>
                        <tr key={i}><td><CurrencyFormat 
                            className = "money"
                            value = {item.Instalment.amnapplicationcapital}
                            displayType={'text'}
                            thousandSeparator={true} /></td></tr>
                      </Table.Cell>
                      <Table.Cell>Số tiền vay</Table.Cell>
                      <Table.Cell>
                        <tr key={i}><td><CurrencyFormat 
                            className = "money"
                            value = {item.Instalment.amnfinancedcapital}
                            displayType={'text'}
                            thousandSeparator={true} /></td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Ngày đến hạn TT kỳ thới</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{getFormattedDate1(item.Instalment.datfirstinstalmentexpiration)}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Phương tiện thanh toán</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Instalment.paymenttype}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Chu kỳ thanh toán gốc</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Instalment.paymentperiodicity}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Số kỳ thanh toán</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Instalment.numtotalinstalment}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Số tiền TB trả trong các tháng tiếp theo</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td><CurrencyFormat 
                            className = "money"
                            value = {item.Instalment.amnmonthlyinstalment}
                            displayType={'text'}
                            thousandSeparator={true} /></td></tr>
                      </Table.Cell>
                      <Table.Cell>Ngày thanh toán gần nhất</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{getFormattedDate1(item.Instalment.datlastpayment)}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Số tháng có số kỳ nợ quá hạn lớn nhất</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.nummonthsinstalmentmax}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Số kỳ nợ quá hạn lớn nhất</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.numinstalmentmax}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Nhóm nợ cao nhất</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.worststatus}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Ngày có nhóm nợ cao nhất</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{getFormattedDate1(item.Contract.datworststatus)}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Ngày có số kỳ nợ quá hạn lớn nhất</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{getFormattedDate1(item.Contract.datinstalmentmax)}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Số tiền nợ quá hạn lớn nhất</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td><CurrencyFormat 
                            className = "money"
                            value = {item.Contract.amnexpiredunpaidinstalmentmax}
                            displayType={'text'}
                            thousandSeparator={true} /></td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Số kỳ thanh toán còn lại</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.numremaininginstalments}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Số tiền còn nợ</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td><CurrencyFormat 
                            className = "money"
                            value = {item.Contract.amnremaining}
                            displayType={'text'}
                            thousandSeparator={true} /></td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Số kỳ nợ quá hạn</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.numexpiredunpaidinstalments}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Số tiền nợ quá hạn</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td><CurrencyFormat 
                            className = "money"
                            value = {item.Contract.amnexpiredunpaidinstalments}
                            displayType={'text'}
                            thousandSeparator={true} /></td></tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Số ngày nợ quá hạn lớn nhất</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{item.Contract.numdayspaymentdelaymax}</td></tr>
                      </Table.Cell>
                      <Table.Cell>Ngày có số ngày nợ quá hạn lớn nhất</Table.Cell>
                      <Table.Cell>
                          <tr key={i}><td>{getFormattedDate1(item.Contract.datdayspaymentdelaymax)}</td></tr>
                      </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
                </Collapse>

                  <Table.HeaderCell> Vay kinh doanh 
                    <IconButton aria-label="expand row"  onClick={() => setOpen7(!open7)}>
                          {open7 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                  </Table.HeaderCell>

                  <Collapse in={open7}>
                <Table singleLine>
                  <Table.Header></Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>Nhóm nợ tại ngày chốt dữ liệu</Table.Cell>
                      <Table.Cell>
                        <tr key={i}>{item.Payment.map((item2,i) => (
                          <tr>{item2.contractstatus}</tr>
                          ))}
                        </tr>
                      </Table.Cell>
                      <Table.Cell>Giai đoạn HĐV</Table.Cell>
                      <Table.Cell>
                        <tr key={i}>{item.Payment.map((item2,i) => (
                          <tr>{item2.contractphase}</tr>
                          ))}
                        </tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Số tiền thanh toán kỳ kỳ tới</Table.Cell>
                      <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr><CurrencyFormat 
                            className = "money"
                            value = {item2.amnrealexpiringinstalment}
                            displayType={'text'}
                            thousandSeparator={true} /></tr>
                            ))}
                          </tr>
                      </Table.Cell>
                      <Table.Cell>Số kỳ thanh toán còn lại</Table.Cell>
                      <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{item2.numremaininginstalments}</tr>
                            ))}
                          </tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Số tiền còn nợ</Table.Cell>
                      <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr><CurrencyFormat 
                            className = "money"
                            value = {item2.amnremaining}
                            displayType={'text'}
                            thousandSeparator={true} /></tr>
                            ))}
                          </tr>
                      </Table.Cell>
                      <Table.Cell>Ngày thanh toán gần nhất</Table.Cell>
                      <Table.Cell>
                        <tr key={i}>{item.Payment.map((item2,i) => (
                          <tr>{getFormattedDate1(item2.datlastpayment)}</tr>
                          ))}
                        </tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Số kỳ nợ quá hạn</Table.Cell>
                      <Table.Cell>
                        <tr key={i}>{item.Payment.map((item2,i) => (
                          <tr>{item2.numexpiredunpaidinstalments}</tr>
                          ))}
                        </tr>
                      </Table.Cell>
                      <Table.Cell>Số tiền nợ quá hạn</Table.Cell>
                      <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr><CurrencyFormat 
                            className = "money"
                            value = {item2.amnexpiredunpaidinstalments}
                            displayType={'text'}
                            thousandSeparator={true} />
                            </tr>
                            ))}
                          </tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Tổng giá trị tài sản bảo đảm cho HĐV</Table.Cell>
                      <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr><CurrencyFormat 
                            className = "money"
                            value = {item2.amnrealguarantee}
                            displayType={'text'}
                            thousandSeparator={true} /></tr>
                            ))}
                          </tr>
                      </Table.Cell>
                      <Table.Cell>Tổng giá trị bảo đảm không dùng tài sản cho HĐV</Table.Cell>
                      <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{item2.amnpersonalguarantee}</tr>
                            ))}
                          </tr>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Số ngày nợ quá hạn</Table.Cell>
                      <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{item2.numdayspaymentdelay}</tr>
                            ))}
                          </tr>
                      </Table.Cell>
                      <Table.Cell> </Table.Cell>
                      <Table.Cell> </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
                </Collapse> 
                </div>
                ))}
                </Collapse>
              </div>
            }
              
              

              {(data.Contracts.Notinstalments.Living) !== 0 &&
                <div className="Notinstalments">
              
                <h5>II. NHÓM: VAY THẤU CHI 
                  <IconButton aria-label="expand row"  onClick={() => setOpen8(!open8)}>
                      {open8 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                  </IconButton>
                </h5>
                <Collapse in={open8}>
                <Table singleLine >
                  <Table.Header>
                    <Table.HeaderCell>Tổng tiền - vay thấu chi
                    </Table.HeaderCell>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell></Table.Cell>
                      <Table.HeaderCell>Vai trò vay chính/ đồng vay</Table.HeaderCell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Hạn mức tín dụng</Table.Cell>
                      <Table.Cell>
                        <CurrencyFormat 
                          className = "money"
                          value = {data.Contracts.Notinstalments.Credit_limit}
                          displayType={'text'}
                          thousandSeparator={true} />
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Dư nợ</Table.Cell>
                        <Table.Cell>
                          <CurrencyFormat 
                            className = "money"
                            value = {data.Contracts.Notinstalments.Residual_Amount}
                            displayType={'text'}
                            thousandSeparator={true} />
                        </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
                

                {data.Contracts.Notinstalments.Notinstalment_list.map((item, i) => (
                <div className='Notinstalment_list'>
                  {/* Contract */}
                  <Table.HeaderCell> Chi tiết: Vay thấu chi
                    <IconButton aria-label="expand row"  onClick={() => setOpen13(!open13)}>
                        {open13 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                  </Table.HeaderCell>
                  <Collapse in={open13}>
                    <Table singleLine>
                      <Table.Body>
                        <Table.Row>
                          <Table.Cell>Mã TCTD</Table.Cell>
                          <Table.Cell>
                            <tr key={i}><td>{item.Contract.organizationowner}</td></tr>
                          </Table.Cell>
                          <Table.Cell>Mã Chi nhánh cấp tín dụng</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{item.Contract.branchowner}</td></tr>
                          </Table.Cell>
                          <Table.Cell> </Table.Cell>
                          <Table.Cell> </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Giai đoạn HĐV tại ngày chốt DL</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{item.Contract.contractphase}</td></tr>
                          </Table.Cell>
                          <Table.Cell>Loại HĐV</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{item.Contract.operationtype}</td></tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Ngày yêu cầu vay</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{getFormattedDate1(item.Contract.datfinancingapplication)}</td></tr>
                          </Table.Cell>
                          <Table.Cell>Ngày bắt đầu HĐV</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{getFormattedDate1(item.Contract.datstartingcontract)}</td></tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Ngày kết thúc theo kế hoạch</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{getFormattedDate1(item.Contract.datclosingcontractprevisional)}</td></tr>
                          </Table.Cell>
                          <Table.Cell>Nhóm nợ tại ngày chốt DL</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{item.Contract.contractstatus}</td></tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Ngày chốt dữ liệu gần nhất</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{getFormattedDate1(item.Contract.datlastupdatebatch)}</td></tr>
                          </Table.Cell>
                          <Table.Cell>Mã khu vực cấp tín dụng</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{item.Contract.areaowner}</td></tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Ngày kết thúc thực tế</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{getFormattedDate1(item.Contract.datclosingcontractreal)}</td></tr>
                          </Table.Cell>
                          <Table.Cell>Loại tiền tệ</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{item.Contract.creditcurrency}</td></tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Cờ tái cơ cấu</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{item.Contract.reorganizedcredit}</td></tr>
                          </Table.Cell>
                        </Table.Row>
                      </Table.Body>
                    </Table>
                  </Collapse>

                  <Table.HeaderCell>  Chi tiết: Vay trả góp
                    <IconButton aria-label="expand row"  onClick={() => setOpen14(!open14)}>
                        {open14 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                  </Table.HeaderCell>

                  {/* Instalment */}
                  <Collapse in={open13}>
                    <Table>
                      <Table.Header></Table.Header>
                      <Table.Body>
                        <Table.Row>
                          <Table.Cell>Hạn mức thấu chi</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>
                              <CurrencyFormat 
                                className = "money"
                                value = {item.Notinstalment.amnworkinggranted}
                                displayType={'text'}
                                thousandSeparator={true} />
                              </td></tr>
                          </Table.Cell>
                          <Table.Cell>Nhóm nợ cao nhất</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{item.Notinstalment.worststatus}</td></tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Ngày có nhóm nợ cao nhất</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{getFormattedDate1(item.Notinstalment.datworststatus)}</td></tr>
                          </Table.Cell>
                          <Table.Cell>Số tiền yêu cầu vay</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td><CurrencyFormat 
                            className = "money"
                            value = {item.Notinstalment.amnapplicationcapital}
                            displayType={'text'}
                            thousandSeparator={true} /></td></tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Ngày thanh toán gần nhất</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{getFormattedDate1(item.Notinstalment.datlastpayment)}</td></tr>
                          </Table.Cell>
                          <Table.Cell>Dư nợ</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>
                                <CurrencyFormat 
                                  className = "money"
                                  value = {item.Notinstalment.amnspendedcredit}
                                  displayType={'text'}
                                  thousandSeparator={true} />
                            </td></tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Tổng giá trị tài sản bảo đảm cho HĐV</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td><CurrencyFormat 
                            className = "money"
                            value = {item.Notinstalment.amnrealguarantee}
                            displayType={'text'}
                            thousandSeparator={true} /></td></tr>
                          </Table.Cell>
                          <Table.Cell>Cờ cho biết HĐV thấu chi có đang vượt hạn mức?</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{item.Notinstalment.exceededcalcolated}</td></tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Số ngày nợ quá hạn lớn nhất</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{item.Notinstalment.numdayspaymentdelaymax}</td></tr>
                          </Table.Cell>
                          <Table.Cell>Ngày có số ngày nợ quá hạn lớn nhất</Table.Cell>
                          <Table.Cell>
                              <tr key={i}><td>{getFormattedDate1(item.Notinstalment.datdayspaymentdelaymax)}</td></tr>
                          </Table.Cell>
                        </Table.Row>
                      </Table.Body>
                    </Table>
                  </Collapse>

                  <Table.HeaderCell> Chi tiết: Hóa đơn
                    <IconButton aria-label="expand row"  onClick={() => setOpen15(!open15)}>
                        {open15 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                  </Table.HeaderCell>
                  
                    {/* Payment */}
                  <Collapse in={open15}>
                    <Table singleLine>
                    <Table.Header></Table.Header>
                      <Table.Body>
                        <Table.Row>
                          <Table.Cell>Nhóm nợ</Table.Cell>
                          <Table.Cell>
                            <tr key={i}>{item.Payment.map((item2,i) => (
                              <tr>{item2.contractstatus}</tr>
                              ))}
                            </tr>
                          </Table.Cell>
                          <Table.Cell>Giai đoạn HĐV</Table.Cell>
                          <Table.Cell>
                            <tr key={i}>{item.Payment.map((item2,i) => (
                              <tr>{item2.contractphase}</tr>
                              ))}
                            </tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Hạn mức thấu chi</Table.Cell>
                          <Table.Cell>
                              <tr key={i}>{item.Payment.map((item2,i) => (
                                <CurrencyFormat 
                                className = "money"
                                value = {item2.amnworkinggranted}
                                displayType={'text'}
                                thousandSeparator={true} />
                                ))}
                              </tr>
                          </Table.Cell>
                          <Table.Cell>Dư nợ</Table.Cell>
                          <Table.Cell>
                              <tr key={i}>{item.Payment.map((item2,i) => (
                                <tr><CurrencyFormat 
                                className = "money"
                                value = {item2.amnspendedcredit}
                                displayType={'text'}
                                thousandSeparator={true} /></tr>
                                ))}
                              </tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Tổng giá trị tài sản bảo đảm cho HĐV</Table.Cell>
                          <Table.Cell>
                            <tr key={i}>{item.Payment.map((item2,i) => (
                              <tr><CurrencyFormat 
                              className = "money"
                              value = {item2.amnrealguarantee}
                              displayType={'text'}
                              thousandSeparator={true} /></tr>
                              ))}
                            </tr>
                          </Table.Cell>
                          <Table.Cell>Tổng giá trị bảo đảm không dùng tài sản cho HĐV</Table.Cell>
                          <Table.Cell>
                            <tr key={i}>{item.Payment.map((item2,i) => (
                              <tr>{item2.amnpersonalguarantee}</tr>
                              ))}
                            </tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Cờ cho biết HĐV thấu chi có đang vượt hạn mức?</Table.Cell>
                          <Table.Cell>
                              <tr key={i}>{item.Payment.map((item2,i) => (
                                <tr>{item2.exceededcalcolated}</tr>
                                ))}
                              </tr>
                          </Table.Cell>
                          <Table.Cell>Tổng giá trị tài sản bảo đảm cho HĐV</Table.Cell>
                          <Table.Cell>
                              <tr key={i}>{item.Payment.map((item2,i) => (
                                <tr><CurrencyFormat 
                                className = "money"
                                value = {item2.amnrealguarantee}
                                displayType={'text'}
                                thousandSeparator={true} /></tr>
                                ))}
                              </tr>
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Tổng giá trị bảo đảm không dùng tài sản cho HĐV</Table.Cell>
                          <Table.Cell>
                              <tr key={i}>{item.Payment.map((item2,i) => (
                                <tr>{item2.amnpersonalguarantee}</tr>
                                ))}
                              </tr>
                          </Table.Cell>
                          <Table.Cell>Số ngày nợ quá hạn</Table.Cell>
                          <Table.Cell>
                              <tr key={i}>{item.Payment.map((item2,i) => (
                                <tr>{item2.numdayspaymentdelay}</tr>
                                ))}
                              </tr>
                          </Table.Cell>
                        </Table.Row>
                      </Table.Body>
                    </Table>
                  </Collapse>
                </div>
                ))}
                </Collapse>
                </div>
              }
              

              {(data.Contracts.Creditcards.Living) !== 0 && 
              <div className="Creditcards">
                <h5>III. NHÓM: THẺ TÍN DỤNG
                  <IconButton aria-label="expand row"  onClick={() => setOpen9(!open9)}>
                      {open9 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                  </IconButton>
                </h5>

                <Collapse in={open9}>
                <Table singleLine >
                  <Table.Header>
                    <Table.HeaderCell>Vai trò vay chính/ đồng vay</Table.HeaderCell>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell></Table.Cell>
                      <Table.HeaderCell>Vai trò vay chính/ đồng vay</Table.HeaderCell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Hạn mức tín dụng</Table.Cell>
                      <Table.Cell>
                        <CurrencyFormat 
                          className = "money"
                          value = {data.Contracts.Creditcards.Credit_limit}
                          displayType={'text'}
                          thousandSeparator={true} />
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Dư nợ</Table.Cell>
                        <Table.Cell>
                          <CurrencyFormat 
                            className = "money"
                            value = {data.Contracts.Creditcards.Residual_Amount}
                            displayType={'text'}
                            thousandSeparator={true} />
                        </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Số tiền nợ quá hạn hiện tại</Table.Cell>
                        <Table.Cell>
                          <CurrencyFormat 
                              className = "money"
                              value = {data.Contracts.Creditcards.Overdue_not_paid_Amount}
                              displayType={'text'}
                              thousandSeparator={true} />
                        </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
                
                
                {data.Contracts.Creditcards.Creditcard_list.map((item, i) => (
                  
                 <div className='Notinstalment_list'>
                  <Table.HeaderCell> Chi tiết: Hợp đồng
                    <IconButton aria-label="expand row"  onClick={() => setOpen10(!open10)}>
                      {open10 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                  </Table.HeaderCell>
                <Collapse in={open10}>
                  <Table singleLine>
                    <Table.Header></Table.Header>
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell>Số tham chiếu</Table.Cell>
                        <Table.Cell>
                          <tr key={i}><td>{item.Contract[" - "]}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Mã TCTD</Table.Cell>
                        <Table.Cell>
                          <tr key={i}><td>{item.Contract.organizationowner}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Mã Chi nhánh cấp tín dụng</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Contract.branchowner}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Giai đoạn HĐV tại ngày chốt DL</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Contract.contractphase}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Loại HĐV</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Contract.operationtype}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Ngày yêu cầu vay</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Contract.datfinancingapplication)}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Ngày bắt đầu HĐV</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Contract.datstartingcontract)}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Ngày kết thúc theo kế hoạch</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Contract.datclosingcontractprevisional)}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Nhóm nợ tại ngày chốt dữ liệu</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Contract.contractstatus}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Ngày chốt dữ liệu gần nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Contract.datlastupdatebatch)}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Mã khu vực cấp tín dụng</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Contract.areaowner}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Ngày kết thúc thực tế</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Contract.datclosingcontractreal)}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Loại tiền tệ</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Contract.creditcurrency}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Cờ tái cơ cấu</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Contract.reorganizedcredit}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                  </Collapse>
                  
                  <Table.Header>
                        <Table.HeaderCell> Chi tiết: Thẻ tín dụng
                          <IconButton aria-label="expand row"  onClick={() => setOpen11(!open11)}>
                            {open11 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                          </IconButton>
                        </Table.HeaderCell>
                    </Table.Header>
                  
                  <Collapse in={open11}>
                  {/* Creditcards */}
                  <Table>
                    <Table.Header></Table.Header>
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell>Số tiền yêu cầu vay</Table.Cell>
                        <Table.Cell>
                          <tr key={i}><td>  <CurrencyFormat 
                              className = "money"
                              value = {item.Creditcards.amnapplicationcapital}
                              displayType={'text'}
                              thousandSeparator={true} /></td></tr>
                        </Table.Cell>
                        <Table.Cell>Ngày chi tiêu gần nhất</Table.Cell>
                        <Table.Cell>
                          <tr key={i}><td>{getFormattedDate1(item.Creditcards.datlastcharge)}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Dư nợ lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>
                              <CurrencyFormat 
                                className = "money"
                                value = {item.Creditcards.amnremainingmax}
                                displayType={'text'}
                                thousandSeparator={true} /></td></tr>
                        </Table.Cell>
                        <Table.Cell>Ngày có dư nợ lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Creditcards.datamountremainingmax)}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Số tiền chi tiêu lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td> <CurrencyFormat 
                              className = "money"
                              value = {item.Creditcards.amnchargemax}
                              displayType={'text'}
                              thousandSeparator={true} /></td></tr>
                        </Table.Cell>
                        <Table.Cell>Ngày có số tiền chi tiêu lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Creditcards.datamountchargemax)}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Số tiền thấu chi lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td><CurrencyFormat 
                              className = "money"
                              value = {item.Creditcards.amnexceededmax}
                              displayType={'text'}
                              thousandSeparator={true} /></td></tr>
                        </Table.Cell>
                        <Table.Cell>Ngày có số tiền thấu chi lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Creditcards.datamountexceededmax)}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Phương tiện thanh toán</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Creditcards.paymenttype}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Số tiền thanh toán tôi thiểu</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td><CurrencyFormat 
                              className = "money"
                              value = {item.Creditcards.amnmonthlyinstalment}
                              displayType={'text'}
                              thousandSeparator={true} /></td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Ngày thanh toán kỳ tới</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Creditcards.datfirstinstalmentexpiration)}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Hạn mức</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>
                              <CurrencyFormat 
                                className = "money"
                                value = {item.Creditcards.amncreditlimitaccount}
                                displayType={'text'}
                                thousandSeparator={true} />
                            </td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Ngày thanh toán gần nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Creditcards.datlastpayment)}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Số tháng có số kỳ nợ quá hạn lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Creditcards.nummonthsinstalmentmax}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Số kỳ nợ quá hạn lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Creditcards.numinstalmentmax}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Số tiền chi tiêu lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td><CurrencyFormat 
                              className = "money"
                              value = {item.Creditcards.amnchargemax}
                              displayType={'text'}
                              thousandSeparator={true} /></td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Ngày có số kỳ nợ quá hạn lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Creditcards.datinstalmentmax)}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Nhóm nợ cao nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Creditcards.worststatus}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Ngày có nhóm nợ cao nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Creditcards.datworststatus)}</td></tr>
                        </Table.Cell>
                        <Table.Cell>Số tiền nợ quá hạn lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td><CurrencyFormat 
                              className = "money"
                              value = {item.Creditcards.amnexpiredunpaidinstalmentmax}
                              displayType={'text'}
                              thousandSeparator={true} /></td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Dư nợ</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>
                              <CurrencyFormat 
                                className = "money"
                                value = {item.Creditcards.amnremaining}
                                displayType={'text'}
                                thousandSeparator={true} /></td></tr>
                        </Table.Cell>
                        <Table.Cell>Số kỳ nợ quá hạn</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Creditcards.numexpiredunpaidinstalments}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Số tiền nợ quá hạn</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>
                              <CurrencyFormat 
                                className = "money"
                                value = {item.Creditcards.amnexpiredunpaidinstalments}
                                displayType={'text'}
                                thousandSeparator={true} /></td></tr>
                        </Table.Cell>
                        <Table.Cell>Số ngày nợ quá hạn lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{item.Creditcards.numdayspaymentdelaymax}</td></tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Ngày có số ngày nợ quá hạn lớn nhất</Table.Cell>
                        <Table.Cell>
                            <tr key={i}><td>{getFormattedDate1(item.Creditcards.datdayspaymentdelaymax)}</td></tr>
                        </Table.Cell>
                        <Table.Cell> </Table.Cell>
                        <Table.Cell> </Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                  </Collapse>

                  <Table.HeaderCell> Chi tiết: Hóa đơn
                    <IconButton aria-label="expand row"  onClick={() => setOpen12(!open12)}>
                      {open12 ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                  </Table.HeaderCell>

                  <Collapse in={open12}>
                  <Table singleLine>
                    <Table.Header></Table.Header>
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell>Nhóm nợ tại ngày chốt dữ liệu</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{item2.contractstatus}</tr>
                            ))}
                          </tr>
                        </Table.Cell>
                        <Table.Cell>Giai đoạn HĐV</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{item2.contractphase}</tr>
                            ))}
                          </tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Cờ thẻ được sử dụng trong tháng</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{item2.flgmonthutilization}</tr>
                            ))}
                          </tr>
                        </Table.Cell>
                        <Table.Cell>Số tiền chi tiêu trong tháng</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr><CurrencyFormat 
                            className = "money"
                            value = {item2.amnmonthlycharge}
                            displayType={'text'}
                            thousandSeparator={true} /></tr>
                            ))}
                          </tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Số lần thẻ được sử dụng trong tháng</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{item2.numutilizationmonth}</tr>
                            ))}
                          </tr>
                        </Table.Cell>
                        <Table.Cell>Ngày chi tiêu gần nhất</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{getFormattedDate1(item2.datlastcharge)}</tr>
                            ))}
                          </tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Dư nợ</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr><CurrencyFormat 
                            className = "money"
                            value = {item2.amnremaining}
                            displayType={'text'}
                            thousandSeparator={true} /></tr>
                            ))}
                          </tr>
                        </Table.Cell>
                        <Table.Cell>Số tiền thấu chi</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr><CurrencyFormat 
                            className = "money"
                            value = {item2.amnexceeded}
                            displayType={'text'}
                            thousandSeparator={true} /></tr>
                            ))}
                          </tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Số kỳ nợ quá hạn</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{item2.numexpiredunpaidinstalments}</tr>
                            ))}
                          </tr>
                        </Table.Cell>
                        <Table.Cell>Số tiền nợ quá hạn</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr><CurrencyFormat 
                            className = "money"
                            value = {item2.amnexpiredunpaidinstalments}
                            displayType={'text'}
                            thousandSeparator={true} /></tr>
                            ))}
                          </tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Số tiền thanh toán tôi thiểu</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr><CurrencyFormat 
                            className = "money"
                            value = {item2.amnmonthlyinstalment}
                            displayType={'text'}
                            thousandSeparator={true} /></tr>
                            ))}
                          </tr>
                        </Table.Cell>
                        <Table.Cell>Ngày thanh toán gần nhất</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{getFormattedDate1(item2.datlastpayment)}</tr>
                            ))}
                          </tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Phương tiện thanh toán</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{item2.paymenttype}</tr>
                            ))}
                          </tr>
                        </Table.Cell>
                        <Table.Cell>Tổng giá trị bảo đảm không dùng tài sản cho HĐV</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{item2.amnpersonalguarantee}</tr>
                            ))}
                          </tr>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Tổng giá trị tài sản bảo đảm cho HĐV</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr><CurrencyFormat 
                            className = "money"
                            value = {item2.amnrealguarantee}
                            displayType={'text'}
                            thousandSeparator={true} /></tr>
                            ))}
                          </tr>
                        </Table.Cell>
                        <Table.Cell>Số ngày nợ quá hạn</Table.Cell>
                        <Table.Cell>
                          <tr key={i}>{item.Payment.map((item2,i) => (
                            <tr>{item2.numdayspaymentdelay}</tr>
                            ))}
                          </tr>
                        </Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                  </Collapse>
                  </div>
                 ))}
                </Collapse>
              </div>
              }
              </div>
              </div>
  )
}
export default ContractSearch;