import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import axios from 'axios';
import authHeader from '../Login/authHeader';
import dayjs from 'dayjs';

export const convertDateTimeFromServerWithFormat = (date, format) => (date ? dayjs(date).format(format) : null);
export const dateBodyTemplate = (rowData, options) => {
  if (rowData[options.field]) {
    return convertDateTimeFromServerWithFormat(rowData[options.field].replace('Z', '+07:00'), 'DD/MM/YYYY');
  } else {
    return rowData[options.field];
  }
};

export const CustomerList = () => {
  const [data1, setData1] = useState();

  useEffect(() => {
    axios.get('https://api.fiindev.com/list',{headers: authHeader()})
    .then(response =>
      setData1(response.data)
      )
  },[]);
  return (
    <div className="datatable-filter" style={{width: '100%'}} >
       <h1 style={{ fontSize:'40px', textAlign:'center'} }>DANH SÁCH KHÁCH HÀNG</h1>
        <DataTable className="p-datatable-gridlines" 
        paginator showGridlines style={{paddingRight:'15px'}}
        rows={10} rowsPerPageOptions={[10,20,50]} currentPageReportTemplate="Showing {first} to {last} of {totalRecords}"
         value={data1} size="small" responsiveLayout="scroll" >
          <Column field="sid" header="Sid" dataType="numeric" sortable filter/>
          <Column field="name" header="Name" sortable filter style={{ minWidth: '10rem' }}/>
          <Column field="dob" body={dateBodyTemplate} header="Dob" dataType="date" sortable filter />
          <Column field="gender" header="Gender" dataType="numeric" sortable filter />
          <Column field="birthtown"  header="Birthtown" sortable filter />
          <Column field="birthcountry" header="Birthcountry" sortable filter />
          <Column field="phone_num" header="Phone_num" sortable filter /> 
          <Column field="type" header="Type" sortable filter /> 
          <Column field="pin" header="Pin" sortable filter /> 
        </DataTable>

  </div>
  
  );
};

export default CustomerList;
