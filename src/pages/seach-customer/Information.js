
import { useEffect, useState } from 'react';
import authHeader from '../Login/authHeader';

import './Information.css'

import axios from 'axios';
import {
    Container,
    Grid,
    Card,
    CardHeader,
    CardContent
  } from '@mui/material';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

function Information () {
   
    
    const dateNow = new Date(); // Creating a new date object with the current date and time
    const year = dateNow.getFullYear(); // Getting current year from the created Date object
    const monthWithOffset = dateNow.getUTCMonth() + 1; // January is 0 by default in JS. Offsetting +1 to fix date for calendar.
    const month = // Setting current Month number from current Date object
    monthWithOffset.toString().length < 2 // Checking if month is < 10 and pre-prending 0 to adjust for date input.
        ? `0${monthWithOffset}`
        : monthWithOffset;
    const date =
    dateNow.getUTCDate().toString().length < 2 // Checking if date is < 10 and pre-prending 0 if not to adjust for date input.
        ? `0${dateNow.getUTCDate()}`
        : dateNow.getUTCDate();
    const materialDateInput = `${year}-${month}-${date}`; 

    const [name1, setName1] = useState ('');
    const [phone1, setPhone1] = useState ('');
    const [IDCard1, setIDCard1] = useState ('');
    const [fullAddress, setFullAddress] = useState ('');
    const [dateOfBirth1, setDateOfBirth1] = useState ('');

    const [data, setData] = useState({ });
    
    var bodyFormData = new FormData();
    
    bodyFormData.append('Name', name1); 
    bodyFormData.append('DateOfBirth', dateOfBirth1); 
    bodyFormData.append('IDCard', IDCard1); 
    bodyFormData.append('FullAddress', fullAddress);
    bodyFormData.append('Phone', phone1);

    
    useEffect(() => {
        axios.post('https://api.fiindev.com/ri_req', bodyFormData,{headers: authHeader()})
        .then(response =>
            setData(response.data),
            localStorage.setItem('dataname', JSON.stringify(data))
        )
    });

    const onButtonClick = (e) => {
        window.location.href = "/customer-search";
    };


    return (
        <div className='info'>
            <Container container maxWidth="xl" className = 'Infor'>
                <Card>
                <CardHeader title="THÔNG TIN KHÁCH HÀNG" />
                <Card sx={{ display: 'flex' }}>
                <CardContent>
                    <Grid
                    container
                    alignItems="stretch"
                    spacing={3}
                    >
                    <Grid component="form"
                                    sx={{
                                    '& .MuiTextField-root': { m: 2, width: '50' },
                                    }}>
                            <TextField sx = {{screenLeft: '100'}}
                                id="text"
                                label="Tên Khách hàng"
                                required
                                onChange={(e)=>setName1(e.target.value)}
                                defaultValue=""
                            />
                            <TextField xs = {12}
                                type="date"
                                required
                                label="Ngày sinh"
                                defaultValue={materialDateInput}
                                onChange={(e)=>setDateOfBirth1(e.target.value)}
                            />
                            <TextField
                                label="Số CMND"
                                required
                                defaultValue=""
                                onChange={(e)=>setIDCard1(e.target.value)}
                            />
                            <TextField
                            id="date"
                            required
                            label="Địa chỉ thường trú"
                            defaultValue=""
                            onChange={(e)=>setFullAddress(e.target.value)}
                        />
                        <TextField
                            id="date"
                            required
                            label="Số điện thoại"
                            defaultValue=""
                            onChange={(e)=>setPhone1(e.target.value)}
                        />
                    </Grid>
                    </Grid>
                </CardContent>
                </Card>
                
            </Card>
            <div  className="Submit">
                    <Button  variant="contained" onClick={onButtonClick}
                        style={{ borderRadius: "4px",marginBottom: "30px" ,width: "100%", height: "40px", fontSize: "20px" }}>
                        Truy vấn
                    </Button>
            </div>
            </Container>
        </div>
    )
}

export default Information;

