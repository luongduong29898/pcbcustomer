import { useState } from 'react';
import './Contract.css'
import {
    Container,
    Grid,
    Card,
    CardHeader,
    CardContent,
    Divider
} from '@mui/material';

import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';

import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';

import Typography from '@mui/material/Typography';

const currencies = [
{
    value: 'hmonth',
    label: 'Thanh toán nửa tháng một lần'
},
{
    value: 'emonth',
    label: 'Thanh toán hàng tháng'
},
{
    value: 'tmonth',
    label: 'Thanh toán hai tháng một lần'
},
{
    value: 'smonth',
    label: 'Thanh toán hàng quý'
},
{
    value: 'hyear',
    label: 'Thanh toán nửa năm một lần'
},
{
    value: 'eyear',
    label: 'Thanh toán hàng năm'
},
{
    value: 'ncircle',
    label: 'Thanh toán không định kỳ'
},
{
    value: 'lcircle',
    label: 'Thanh toán cuối kỳ'
}
];



function Contract() {
    const [currency, setCurrency] = useState('hmonth');

    const handleChange = (event) => {
        setCurrency(event.target.value);
    };

    const dateNow = new Date(); // Creating a new date object with the current date and time
    const year = dateNow.getFullYear(); // Getting current year from the created Date object
    const monthWithOffset = dateNow.getUTCMonth() + 1; // January is 0 by default in JS. Offsetting +1 to fix date for calendar.
    const month = // Setting current Month number from current Date object
    monthWithOffset.toString().length < 2 // Checking if month is < 10 and pre-prending 0 to adjust for date input.
        ? `0${monthWithOffset}`
        : monthWithOffset;
    const date =
    dateNow.getUTCDate().toString().length < 2 // Checking if date is < 10 and pre-prending 0 if not to adjust for date input.
        ? `0${dateNow.getUTCDate()}`
        : dateNow.getUTCDate();
    const materialDateInput = `${year}-${month}-${date}`; 
    
    const [value, setValue] = useState('credit');

    
    const handleRadioChange = (event) => {
        setValue(event.target.value);
    };

    return (
        <div className='contract'>
                <h1 style={{ fontSize:'40px', textAlign:'center'} }>TRUY VẤN THÔNG TIN HỢP ĐỒNG VAY</h1>
            <Container maxWidth="xl" className='Container'>
                <Card>
                <CardHeader title="THÔNG TIN HỢP ĐỒNG" />
                <Card sx={{ display: 'flex' }}>
                    <CardContent>
                        <Grid
                        container
                        alignItems="stretch"
                        spacing={3} 
                        >
                            <Grid item xs={{
                                '& .MuiTextField-root': { m: 1, width: '50' }
                            }}>
                            <Card>
                            <Typography sx={{ marginLeft: 2, fontSize: 15, fontWeight: 600 }}>LOẠI HỢP ĐỒNG</Typography>
                                <Divider />
                                    <RadioGroup className='radiobutton'
                                        collumn
                                        aria-label="gender"
                                        aria-labelledby="demo-controlled-radio-buttons-group"
                                        name="controlled-radio-buttons-group"
                                        alignItems="stretch"
                                        onChange={handleRadioChange}
                                        size="small" 
                                    >
                                    <FormControlLabel 
                                        value="credit"
                                        control={<Radio className='radiobutton'/>}
                                        label="Thẻ tín dụng (Visa, Master Card, JCB, ...)"
                                    />
                                    <FormControlLabel 
                                        value="pn"
                                        control={<Radio className='radiobutton'/>}
                                        label="Vay cá nhân (Vay tiêu dùng)" 
                                    />
                                    </RadioGroup>
                                </Card>
                                </Grid>
                                <Grid item xs={12}>
                                    <div>
                                        <TextField className='datedata'
                                            id="date"
                                            type="date"
                                            label="Ngày yêu cầu cấp tín dụng"
                                            defaultValue= {materialDateInput}
                                        />
                                    </div>
                                </Grid>
                        </Grid>
                    </CardContent>
                    {value === 'credit' &&
                        <>
                            <Grid item  xs={{'& .MuiTextField-root': { m: 1, width: '50' }}} >
                            <Card  className='card1'>
                            <CardContent  >
                                <Box className = 'credit'
                                    component="form"
                                    
                                    sx={{
                                    '& .MuiTextField-root': { m: 1, width: '50' },
                                    }}
                                    noValidate
                                    autoComplete="off"
                                >
                                    <TextField
                                        required
                                        id="outlined-required"
                                        label="Số Tiền Vay"
                                        defaultValue=""
                                    />
                                    <TextField sx={{width: 300}}
                                        required
                                        id="outlined-required"
                                        label="Tổng số kỳ thanh toán gốc và lãi"
                                        defaultValue=""
                                    />
                                    <TextField
                                        id="outlined-select-currency"
                                        select
                                        label="Chu kỳ thanh toán"
                                        value={currency}
                                        onChange={handleChange} 
                                        xs={{width: '100'}}
                                    >
                                        {currencies.map((option) => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                            ))}
                                    </TextField>
                                </Box>
                            </CardContent>
                            </Card>
                            </Grid> 
                        </>
                    }
                    {value === 'pn' &&
                        <>
                            <Grid  >
                            <Card className='card2'>
                            <CardContent>
                                <Box
                                    component="form"
                                    sx={{
                                    '& .MuiTextField-root': { m: 1, width: '50', left: '40'},
                                    
                                    }}
                                    noValidate
                                    autoComplete="off"
                                >
                                    <TextField
                                        required
                                        id="outlined-required"
                                        label="Hạn mức tín dụng"
                                        defaultValue=""
                                    />
                                    <div>
                                        <TextField
                                            id="outlined-select-currency"
                                            select
                                            label="Chu kỳ thanh toán gốc"
                                            value={currency}
                                            onChange={handleChange} 
                                            xs={{
                                                '& .MuiTextField-root': { m: 1, width: '50' }
                                            }}
                                        >
                                            {currencies.map((option) => (
                                                <MenuItem key={option.value} value={option.value}>
                                                    {option.label}
                                                </MenuItem>
                                                ))}
                                        </TextField>
                                    </div>
                                </Box>
                            </CardContent>
                            </Card>
                            </Grid>
                        </>
                    }
                </Card>
            </Card>
            </Container>
            
        </div>
    )
}

export default Contract;