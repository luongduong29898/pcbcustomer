import { TextField } from "@mui/material";
import { useState, useEffect } from "react";
import authHeader from '../Login/authHeader';
import axios from 'axios';
import MenuItem from '@mui/material/MenuItem';
import { useNavigate } from 'react-router-dom';
import './quickSearch.css'


const currencies = [
    {
        value: 'phone',
        label: 'Tìm kiếm bằng số điện thoại'
    },
    {
        value: 'pin',
        label: 'Tìm kiếm bằng số CMND'
    }
];

function QuickSearch() {
    const [data2, setData2] = useState();
    const [currency, setCurrency] = useState('pin');
    const [value, setValue] = useState('');
    const [phone, setPhone] = useState('');
    const [pin, setPin] = useState('');

    const handleChange = (event) => {
        setCurrency(event.target.value);
    };
    
   const changeValue = () =>{
    if (currency === 'phone'){
        setValue(currency+'/'+phone);
    }
    else if (currency === 'pin'){  
        setValue(currency+'/'+pin);
    }
    }
    useEffect(()=>{
        changeValue();
        axios.get('https://api.fiindev.com/' + value,{headers: authHeader()})
        .then(response =>
          setData2(response.data),
          localStorage.setItem('dataname', JSON.stringify(data2)),
          
      )
    console.log(value)
    })

    let navigate = useNavigate();

   const onButtonClick = (e) => {
    console.log(data2)
     
    // navigate("/customer-search");
}   
    
    return (
        <div className="background">
            <div className="d-flex justify-content-center align-items-center gradient">
                <div className="form" >
                    <div className="mb-6"><h4>Tìm Kiếm Nhanh</h4></div>
                    <div className="choose">
                        <TextField className="p-inputgroup"
                            id="outlined-select-currency"
                            select
                            label="Chọn cách thức tìm"
                            value={currency}
                            onChange={handleChange} 
                        >
                            {currencies.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                            ))}
                        </TextField>
                    </div>
                 
                        {currency === 'phone' &&
                        <TextField  className="p-inputgroup"
                            required
                            id="outlined-required"
                            label="Nhập số điện thoại cần tìm "
                            defaultValue=""
                            onChange={(e)=>setPhone(e.target.value)}>
                        </TextField>
                        }
                        {currency === 'pin' &&
                        <TextField  className="p-inputgroup"
                            required
                            id="outlined-required"
                            label="Nhập số CMND cần tìm"
                            defaultValue=""
                            onChange={(e)=>setPin(e.target.value)}>
                        </TextField>
                        }
                    <div className="d-grid" >
                        <button className="btn btn-primary" type="submit" onClick={onButtonClick}>Search</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default QuickSearch;