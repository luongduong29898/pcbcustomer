import './header.css';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import {Menubar} from 'primereact/menubar';
import * as IoIcons from 'react-icons/io';

const Header = () => {
  
  
  const home = <i className='home'><img alt='HOME' src='./logopcb.png' height="40px"></img></i>

  const items = [
        {
          className:"home",
          icon: home,
          command: () => {
            window.location.href = "/";
          }
        }
  ];

  const itemsupdate = [
    {
      className:"home",
      icon: home,
    },
    {
      className:"topbar-right",
      label: 'DSKH',
      icon: 'pi pi-users',
      command: () => {
        window.location.href = "/customer-list";
      }
    },
    {
      className:"topbar-right",
      label: 'Truy vấn nhanh',
      
      icon: 'pi pi-search',
      command: () => {
        window.location.href = "/quick-search";
      }
    },
    {
      className:"topbar-right",
      label: 'Truy vấn HĐV',
      icon: 'pi pi-search',
      command: () => {
        window.location.href = "/search-customer";
      }
    },
    {
      className:"logout",
      label: 'Log out',
      icon: IoIcons.IoIosLogOut,
      command: () => {
        localStorage.removeItem("user");
        window.location.href = "/";
      }
    }
  ];

  let temp;
  if(localStorage.getItem("user")){
    temp = itemsupdate;
  }
  else{
    temp = items;
  }

  return (
    <div >
      <Menubar className="menu"  model={temp}> </Menubar>
    </div>
  );
};

export default Header;
