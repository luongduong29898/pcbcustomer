import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Header from './header/header';
import { Route, Routes } from 'react-router-dom';
import { CustomerList } from './pages/customer-list/customer-list';
import CustomerSearch  from './pages/customer-search/customer-search'
import Login from './pages/Login/Login'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Searchcustomer from './pages/seach-customer/search-customer';
import QuickSearch from './pages/quicksearch/quickSearch';

function App() {
  return (
    <div id="app">
      <Header />
      <Routes>
        <Route path="/" element={ <Login />}></Route>
        <Route path="/customer-list" element={ <CustomerList />}></Route>
        <Route path="/customer-search" element={ <CustomerSearch />}></Route>
        <Route path="/search-customer" element={<Searchcustomer />} />
        <Route path="/quick-search" element={<QuickSearch />} />
        
      </Routes>
    </div>
  );
}

export default App;
